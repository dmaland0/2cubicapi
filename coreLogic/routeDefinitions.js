/****************************************
*****************************************
Dependencies*/

//Builtin And Third-Party Modules
const u = require("./utilities/jsUtilities").JSUtilities;
//Custom Modules
const usersController = require("./controllers/usersController");
const uploadController = require("./controllers/uploadController");
const loginThrottler = require("./utilities/loginThrottler");
const managedUIFilesController = require("./controllers/managedUIFilesController");
const logController = require("./controllers/logController");
const userRoutes = require("../routeDefinitions");

/****************************************
*****************************************
Module Logic*/

//VERY IMPORTANT: These route definitions are available to anyone that hits "/api/routeDump/". Don't put anything in them that ought to be a secret!

exports.getRoutes = [];

exports.postRoutes = [];

exports.putRoutes = [];

exports.deleteRoutes = [];

//Merge any routes defined in routeDefinitions.
u.forIn(userRoutes.getRoutes, function(index, route) {
  exports.getRoutes.push(route);
});

u.forIn(userRoutes.postRoutes, function(index, route) {
  exports.postRoutes.push(route);
});

u.forIn(userRoutes.putRoutes, function(index, route) {
  exports.putRoutes.push(route);
});

u.forIn(userRoutes.deleteRoutes, function(index, route) {
  exports.deleteRoutes.push(route);
});

/****************************************
*****************************************
*/

var coreGetRoutes = [

  {
    route: "users/all/?",
    beforeAction: [usersController.authenticateWithToken, usersController.requireSuperuser],
    action: usersController.getMany,
    description: "Retrieve many users. Because there could be an enormous number of users stored, only the first 100 users with an ID greater than the parameter are retrieved."
  },

  {
    route: "users/?",
    beforeAction: [usersController.authenticateWithToken, usersController.requireSuperuser],
    action: usersController.getOne,
    description: "Retrieve a single user using their ID."
  },

  {
    route: "users/getByEmail/?",
    beforeAction: [usersController.authenticateWithToken, usersController.requireSuperuser],
    action: usersController.getByEmail,
    description: "Retrieve a single user using their email."
  },

  {
    route: "users/getSelf/",
    beforeAction: [usersController.authenticateWithToken],
    action: usersController.getSelf,
    description: "Retrieve details of the currently acting user."
  },

  {
    route: "users/validateToken/",
    beforeAction: [],
    action: usersController.authenticateWithToken,
    description: "Determine if a user's cubicAPIToken (submitted as a cookie) is valid."
  },

  {
    route: "logs/all/?",
    beforeAction: [usersController.authenticateWithToken, usersController.requireSuperuser],
    action: logController.getMany,
    description: "Retrieve many log entries. Because an enormous number of records could possibly be available, only returns the first 100 records following the ID passed as a parameter. Only superusers can retrieve the logs."
  },

  {
    route: "logs/?",
    beforeAction: [usersController.authenticateWithToken, usersController.requireSuperuser],
    action: logController.getOne,
    description: "Retrieve a single log entry. Restricted to superusers."
  }

];

var corePostRoutes = [

  {
    route: "users/",
    beforeAction: [],
    action: usersController.create,
    requiredFields: ["email", "password", "repeatPassword"],
    description: "Create a new user."
  }

];

var corePutRoutes = [

  {
    route: "login/",
    beforeAction: [loginThrottler.throttle],
    action: usersController.login,
    requiredFields: ["email", "password"],
    description: "Login with an email and password."
  },

  {
    route: "users/?",
    beforeAction: [usersController.authenticateWithToken, usersController.requireSuperuser],
    action: usersController.update,
    requiredFields: ["email", "isSuperuser", "mustChangePassword"],
    description: "Update a user's information. Restricted to superusers."
  },

  {
    route: "users/changePassword/",
    beforeAction: [],
    action: usersController.changePassword,
    requiredFields: ["email", "oldPassword", "newPassword", "repeatNewPassword"],
    description: "Change a user's password, using the old password for authentication."
  },

  {
    route: "users/changePasswordWithKey/",
    beforeAction: [],
    action: usersController.changePasswordWithKey,
    requiredFields: ["key", "newPassword", "repeatNewPassword"],
    description: "Change a user's password, using a reset key for authentication."
  },

  {
    route: "users/confirmRegistration/",
    beforeAction: [],
    action: usersController.confirmRegistration,
    requiredFields: ["key"],
    description: "Confirm the receipt of a registration key so that a user can log in normally."
  },

  {
    route: "users/authorizeDevice/",
    beforeAction: [],
    action: usersController.authorizeDevice,
    description: "Use an authorization code to allow a device/ browser access on the system."
  },

  {
    route: "passwordResetSendEmail/",
    beforeAction: [],
    action: usersController.passwordResetSendEmail,
    requiredFields: ["email"],
    description: "Send a password reset token via email if the specified email belongs to a registered user."
  }

];

var coreDeleteRoutes = [

  {
    route: "users/?",
    beforeAction: [usersController.authenticateWithToken, usersController.requireSuperuser],
    action: usersController.delete,
    description: "Remove a user."
  }

];

u.forIn(coreGetRoutes, function(index, route) {
  exports.getRoutes.push(route);
});

u.forIn(corePostRoutes, function(index, route) {
  exports.postRoutes.push(route);
});

u.forIn(corePutRoutes, function(index, route) {
  exports.putRoutes.push(route);
});

u.forIn(coreDeleteRoutes, function(index, route) {
  exports.deleteRoutes.push(route);
});

/****************************************
*****************************************
Required routes are mandatory Cubes behaviors. They are kept here to keep routeDefinitions.js cleaner, but are always included.*/

var requiredGetRoutes = [

  {
    route: "managedUIFiles/",
    beforeAction: [],
    action: managedUIFilesController.getAll,
    description: "Retrieve all managed UI files. Always publicly available."
  },

  {
    route: "managedUIFiles/?",
    beforeAction: [],
    action: managedUIFilesController.getOne,
    description: "Retrieve a managed UI file by filename. Always publicly available."
  }

];

var requiredPostRoutes = [

  {
    route: "upload/",
    beforeAction: [usersController.authenticateWithToken],
    action: uploadController.upload,
    requiredFields: ["file", "fileName"],
    description: "Upload a file to the server, to be stored in the uploads directory. Upload support is very basic; The controller assumes everything is a binary file requiring base64 encoding."
  },

  {
    route: "managedUIFiles/",
    beforeAction: [usersController.authenticateWithToken, usersController.requireSuperuser],
    action: managedUIFilesController.create,
    requiredFields: ["name", "content"],
    description: "Create a new managed UI file. New files are unpublished by default."
  }

];

var requiredPutRoutes = [

  {
    route: "managedUIFiles/?",
    beforeAction: [usersController.authenticateWithToken, usersController.requireSuperuser],
    action: managedUIFilesController.update,
    requiredFields: ["content", "published"],
    description: "Update a managed UI file. The parameter is the name of the file you wish to change."
  }

];

var requiredDeleteRoutes = [

  {
    route: "managedUIFiles/?",
    beforeAction: [usersController.authenticateWithToken, usersController.requireSuperuser],
    action: managedUIFilesController.delete,
    description: "Remove a managed UI file. The parameter is the name of the file you wish to change. Deletion is permanent and can't be undone. Unpublishing a file is much safer."
  }

];

u.forIn(requiredGetRoutes, function(index, route) {
  exports.getRoutes.push(route);
});

u.forIn(requiredPostRoutes, function(index, route) {
  exports.postRoutes.push(route);
});

u.forIn(requiredPutRoutes, function(index, route) {
  exports.putRoutes.push(route);
});

u.forIn(requiredDeleteRoutes, function(index, route) {
  exports.deleteRoutes.push(route);
});

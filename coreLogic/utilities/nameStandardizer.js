/****************************************
*****************************************
Dependencies*/

//Builtin And Third-Party Modules

//Custom Modules

/****************************************
*****************************************
Module Logic*/

/*
standardize

input: string, text to be standardized.

Returns a standardized version of the input string.
*/
exports.standardize = function(input) {

  if (!input) {

    //If the input is missing or empty, return an empty string.
    return "";

  } else {
    //Restrict the string to 100 characters.
    var shortened = input.substring(0,100);
    //Globally replace all spaces with underscores.
    shortened = shortened.replace(/[ ]/g, "_");
    //Globally replace anything not a letter, number, underscore, hyphen, or period with an empty string.
    shortened = shortened.replace(/[^a-zA-Z0-9-_.]/g, "");
    return shortened;
  }

};

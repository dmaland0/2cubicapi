/****************************************
*****************************************
Dependencies*/

//Builtin And Third-Party Modules
const u = require("./jsUtilities").JSUtilities;
//Custom Modules
const environmentVariables = require("../../environmentVariables").environmentVariables;
const dataInstance = require("../dataInstance/coreInstanceDefinitions");

/****************************************
*****************************************
Module Logic*/

/*
throttle

callObject {
  parameters: array of strings, the "?" variables in an API route
  payload: object, data submitted to the server
  cookies: array of strings, the cookies found in the request
  requestHeaders: object, the headers from the request
  request: object, the entire, raw request received by the server
}

resultCallback: function, a callback
  parameter: boolean, whether the operation was successful
  parameter: integer, the relevant HTTP status code
  parameter: array of strings, the cookies to be set
  parameter: string, a message about the operation
  parameter: null

*/
exports.throttle = async function(callObject, resultCallback) {

  //Get all failures related to either the IP address in use or the email being entered.
  var ip = callObject.request.connection.remoteAddress;
  var email = callObject.payload.email;

  var failedLogins = new dataInstance.failedLogin();
  failedLogins = await failedLogins.openAll();
  var instances = [];

  u.forIn(failedLogins.instances, function(key, instance) {
    if (instance.data.ip === ip || instance.data.email === email) {
      instances.push(instance);
    }
  });

  if (instances.length >= environmentVariables.maxFailedLoginsBeforeThrottle) {

    //Continue if there are too many failed logins.
    var latestAttempt = instances[instances.length - 1];
    var latestAttemptTime = new Date(latestAttempt.data.time);
    var unlockTime = latestAttemptTime.getTime() + environmentVariables.throttledLoginTimeout * 60 * 1000;
    var now = new Date();

    if (now.getTime() < unlockTime) {

      //Reject the request if too little time has elapsed.
      var unlockAt = u.makeSQLDatetime(unlockTime);
      resultCallback(false, 403, [], "You have too many failed login attempts, and your requests are being throttled until server time: " + unlockAt, null);

    } else {

      //If enough time has elapsed, perform a reset.
      u.forIn(instances, async function(key, instance) {

        var deletion = await instance.delete();

        if (deletion !== true) {
          resultCallback(false, 500, [], "Couldn't remove a failed login: " + deletion, null);
          failedLogins = null;
        }

      });

      if (failedLogins) {
        resultCallback(true, 200, [], "Failed logins were removed.", null);
      }

    }

  } else {
    resultCallback(true, 200, [], "Failed logins below lockout threshold.", null);
  }

};

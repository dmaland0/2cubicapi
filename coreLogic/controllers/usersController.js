/****************************************
*****************************************
Dependencies*/

//Builtin And Third-Party Modules
const u = require("../utilities/jsUtilities").JSUtilities;
const crypto = require("crypto");
//Custom Modules
const environmentVariables = require("../../environmentVariables").environmentVariables;
const dataInstance = require("../dataInstance/coreInstanceDefinitions");
const mailer = require("../utilities/mailer");

/****************************************
*****************************************
Module Logic*/

/*
hashPassword

password: string, the actual password to be hashed

salt: string, a value to append to the password to make breaking the hashed value more difficult

Returns a hash digest.
*/
function hashPassword(password, salt) {

  const hash = crypto.createHash("sha512");
  hash.update(password + salt, "utf8");
  return hash.digest("hex");

}

/*
cubicAPITokenExtractor

cookieArray: array of strings, the cookies from a request

Returns an cubicAPIToken, or null.

*/
function cubicAPITokenExtractor (cookieArray) {

  var foundToken = false;

  u.forIn(cookieArray, function(index, cookie) {

    if (cookie.indexOf("cubicAPIToken=") >= 0) {
      foundToken = cookie;
    }

  });

  if (foundToken) {
    return foundToken.substring(foundToken.indexOf("=")+1);
  } else {
    return null;
  }

}

/*
cubicAPIDeviceKeyExtractor

cookieArray: array of strings, the cookies from a request

Returns an cubicAPIDeviceKey, or null.

*/
function cubicAPIDeviceKeyExtractor (cookieArray) {

  var foundKey = false;

  u.forIn(cookieArray, function(index, cookie) {

    if (cookie.indexOf("cubicAPIDeviceKey=") >= 0) {
      foundKey = cookie;
    }

  });

  if (foundKey) {
    return foundKey.substring(foundKey.indexOf("=")+1);
  } else {
    return null;
  }

}

/*
sendDeviceKeyEmail

key: string, a device key.

callback: function, a callback
  parameter: boolean, whether the operation was successful
  parameter: string, a message regarding the operation

*/
function sendDeviceKeyEmail(key, request, user, emailCallback) {

  var pattern = /:.+/g;
  var host = request.headers.host.replace(pattern, "");
  var origin = request.headers.origin;

  //Some browsers don't set the origin header on POST requests, so we need a bit of a kludge.
  if (!origin) {
    origin = request.headers.referer;
  } else {
    origin = origin + "/";
  }

  var data = {
    from: "Email Notifications <api-noreply@" + host + ">",
    to: user.data.email,
    subject: "Device Key Authorization",
    text: "A login attempt was made on a device or browser that was unrecognized. A device key was created, which you can attempt to authorize at " + origin + "authorizeDevice?=" + key + " Do not authorize a device unless you yourself tried to login and were unable to complete the process. If this message is unexpected, someone has very likely discovered your password, and you should change or reset it immediately!"
  };

  mailer.sendMail(data, function(success, message) {

    if (!success) {
      emailCallback(false, message)
    } else {
      emailCallback(true, message);
    }

  });

}

/*
sendConfirmationEmail

key: string, the user's registration key.

callback: function, a callback
  parameter: boolean, whether the operation was successful
  parameter: string, a message regarding the operation

*/
function sendConfirmationEmail(key, request, user, emailCallback) {

  var pattern = /:.+/g;
  var host = request.headers.host.replace(pattern, "");
  var origin = request.headers.origin;

  //Some browsers don't set the origin header on POST requests, so we need a bit of a kludge.
  if (!origin) {
    origin = request.headers.referer;
  } else {
    origin = origin + "/";
  }

  var data = {
    from: "Email Notifications <api-noreply@" + host + ">",
    to: user.data.email,
    subject: "Registration Confirmation",
    text: "Your user registration at " + host + " is almost complete. You can confirm your registration at " + origin + "confirmRegistration?=" + key + " If you did not intend to register on the system, simply delete this email - nobody can log in as you without completing this confirmation."
  };

  mailer.sendMail(data, function(success, message) {

    if (!success) {
      emailCallback(false, message)
    } else {
      emailCallback(true, message);
    }

  });

}

/*
sendResetEmail

key: string, the user's password reset key.

callback: function, a callback
  parameter: boolean, whether the operation was successful
  parameter: string, a message regarding the operation

*/
function sendResetEmail(finalKey, request, email, emailCallback) {

  var pattern = /:.+/g;
  var host = request.headers.host.replace(pattern, "");

  var data = {
    from: "Email Notifications <api-noreply@" + host + ">",
    to: email,
    subject: "Password Reset",
    text: "A password reset was requested for your account at " + host + ". The reset key is: " + finalKey + "\n\nYour password can not be reset without this key. If you've remembered your password, simply log in normally and the key will be discarded."
  };

  mailer.sendMail(data, function(success, message) {

    if (!success) {
      emailCallback(false, message)
    } else {
      emailCallback(true, message);
    }

  });

}

/*
getMany

callObject {
  parameters: array of strings, the "?" variables in an API route
  payload: object, data submitted to the server
  cookies: array of strings, the cookies found in the request
  requestHeaders: object, the headers from the request
}

resultCallback: function, a callback
  parameter: boolean, whether the operation was successful
  parameter: integer, the relevant HTTP status code
  parameter: array of strings, the cookies to be set
  parameter: string, a message about the operation
  parameter: array, a collection of user objects

*/
exports.getMany = async function (callObject, resultCallback) {

  var startIndex = callObject.parameters[0];
  var users = new dataInstance.user();
  users = await users.openMany(startIndex, 100);

  u.forIn(users.instances, function(index, user) {
    user.protectSecretFields();
  });

  if (!u.length(users.instances)) {
    resultCallback(false, 404, [], "No users found when utilizing starting ID " + startIndex, null);
  } else {
    resultCallback(true, 200, [], "Users were retrieved.", users);
  }

};

/*
getOne

callObject {
  parameters: array of strings, the "?" variables in an API route
  payload: object, data submitted to the server
  cookies: array of strings, the cookies found in the request
  requestHeaders: object, the headers from the request
}

resultCallback: function, a callback
  parameter: boolean, whether the operation was successful
  parameter: integer, the relevant HTTP status code
  parameter: array of strings, the cookies to be set
  parameter: string, a message about the operation
  parameter: object, a user object

*/
exports.getOne = async function(callObject, resultCallback) {

  var user = new dataInstance.user();
  user = await user.open(callObject.parameters[0]);

  user.protectSecretFields();

  if (!user.id) {
    resultCallback(false, 404, [], "No users found with ID " + callObject.parameters[0], null);
  } else {
    resultCallback(true, 200, [], "A user was retrieved.", user);
  }

};

/*
getByToken

callObject {
  parameters: array of strings, the "?" variables in an API route
  payload: object, data submitted to the server
  cookies: array of strings, the cookies found in the request
  request: object, the entire, raw request received by the server
  requestHeaders: object, the headers from the request
}

resultCallback: function, a callback
  parameter: boolean, whether the operation was successful
  parameter: integer, the relevant HTTP status code
  parameter: array of strings, the cookies to be set
  parameter: string, a message about the operation
  parameter: object, a user object

*/
exports.getSelf = async function(callObject, resultCallback) {

  var token = cubicAPITokenExtractor(callObject.cookies);
  var user = new dataInstance.user();
  user = await user.findAll("accessToken", token);

  if (!u.length(user.instances)) {
    resultCallback(false, 404, [], "The supplied cubicAPIToken couldn't be matched with an available user.", null);
  } else {

    var foundUser = null;

    u.forIn(user.instances, function(index, instance) {
      foundUser = instance;
    });

    if (!foundUser) {
      resultCallback(false, 404, [], "The supplied cubicAPIToken couldn't be matched with an available user.", null);
    } else {
      foundUser.protectSecretFields();
      resultCallback(true, 200, [], "Retrieved the currently acting user.", foundUser);
    }

  }

};

/*
getByEmail

callObject {
  parameters: array of strings, the "?" variables in an API route
  payload: object, data submitted to the server
  cookies: array of strings, the cookies found in the request
  requestHeaders: object, the headers from the request
}

resultCallback: function, a callback
  parameter: boolean, whether the operation was successful
  parameter: integer, the relevant HTTP status code
  parameter: array of strings, the cookies to be set
  parameter: string, a message about the operation
  parameter: object, a user object

*/
exports.getByEmail = async function(callObject, resultCallback) {

  var user = new dataInstance.user();
  user = await user.findFirst("email", callObject.parameters[0]);

  user.protectSecretFields();

  if (!user.id) {
    resultCallback(false, 404, [], "No users found with email " + callObject.parameters[0], null);
  } else {
    resultCallback(true, 200, [], "A user was retrieved.", user);
  }

};

/*
create

callObject {
  parameters: array of strings, the "?" variables in an API route
  payload: object, data submitted to the server
  cookies: array of strings, the cookies found in the request
  requestHeaders: object, the headers from the request
  request: object, the entire request received by the server
}

resultCallback: function, a callback
  parameter: boolean, whether the operation was successful
  parameter: integer, the relevant HTTP status code
  parameter: array of strings, the cookies to be set
  parameter: string, a message about the operation
  parameter: object, a user object

*/
exports.create = async function(callObject, resultCallback) {

  if (environmentVariables.registrationDisabled) {
    resultCallback(false, 403, [], "This system is currently disallowing new user registrations.", null);
    return false;
  }

  var user = new dataInstance.user();
  var email = callObject.payload.email;

  if (!u.emailSeemsValid(email)) {
    resultCallback(false, 400, [], "The specified email doesn't seem to be a valid address.", null);
  } else {
    var existingUser = await user.findFirst("email", email);
    if (existingUser.id) {
      resultCallback(false, 400, [], "The specified email is already in use.", null);
    } else {
      if (callObject.payload.password !== callObject.payload.repeatPassword) {
        resultCallback(false, 400, [], "The two submitted passwords don't match.", null);
      } else {

        var lastUser = new dataInstance.user();
        lastUser = await lastUser.openLast();

        //The first user to register is given superuser privileges.
        if (!lastUser.id) {
          user.data.isSuperuser = 1;
        } else {
          user.data.isSuperuser = 0;
        }

        user.data.salt = crypto.randomBytes(128).toString("hex");
        user.data.hashedPassword = hashPassword(callObject.payload.password, user.data.salt);
        user.data.mustChangePassword = 0;
        user.data.createdAt = u.makeSQLDatetime(new Date());
        user.data.email = email;

        var save = await user.save();
        user = await user.findFirst("email", email);

        if (save !== true || !user.id) {
          resultCallback(false, 500, [], "The new user couldn't be saved and retrieved properly.", save);
        } else {

          //Create a key for registration confirmation.
          var key = crypto.randomBytes(128).toString("hex");
          var modifier = crypto.randomBytes(64).toString("hex");
          var finalKey = hashPassword(key, modifier);
          var registrationKey = new dataInstance.registrationKey();
          registrationKey.data.key = finalKey;
          registrationKey.data.userID = user.id;

          save = await registrationKey.save();

          if (save !== true) {
            resultCallback(false, 500, [], "The new user was saved, but the registration key wasn't saved.", save);
          } else {

            key = crypto.randomBytes(128).toString("hex");
            modifier = crypto.randomBytes(64).toString("hex");
            finalKey = hashPassword(key, modifier);
            var deviceKey = new dataInstance.deviceKey();
            deviceKey.data.key = finalKey;
            deviceKey.data.userID = user.id;
            deviceKey.data.authorized = 1;
            deviceKey.data.authorizationCode = "";

            save = await deviceKey.save();

            if (save !== true) {
              resultCallback(false, 500, [], "The new user and registration key were saved, but the device key wasn't saved.", save);
            } else {

              sendConfirmationEmail(registrationKey.data.key, callObject.request, user, function(success, message) {

                //Set a cookie to authorize the device for 10 years.
                var deviceKeyCookie = "cubicAPIDeviceKey=" + finalKey + "; Secure; " + "Max-Age=" + (3.154 * 100000000) + ";" + " Path=/";
                var infoObject = {createdUser: user.id, developerMessage: "If this frontend can't send cookies to the API, you will need to manually store and send the included cubicAPIDeviceKey value in an Authorization header. The Authorization header should be of the form: Bearer cubicAPIToken=[value]; cubicAPIDeviceKey=[value]", cubicAPIDeviceKey: deviceKeyCookie.split("; ")[0]};

                if (!success) {
                  resultCallback(false, 500, [deviceKeyCookie], "User " + user.id + " was created successfully, and a confirmation key was generated, but the confirmation email doesn't appear to have been sent successfully: " + message, infoObject);
                } else {
                  resultCallback(true, 200, [deviceKeyCookie], "User " + user.id + " was created successfully, and a confirmation key was generated. Confirmation is required before a login can be attempted." + message, infoObject);
                }

              });

            }

          }

        }

      }

    }

  }

};

/*
login

callObject {
  parameters: array of strings, the "?" variables in an API route
  payload: object, data submitted to the server
  cookies: array of strings, the cookies found in the request
  requestHeaders: object, the headers from the request
  request: object, the complete request received by the server
}

resultCallback: function, a callback
  parameter: boolean, whether the operation was successful
  parameter: integer, the relevant HTTP status code
  parameter: array of strings, the cookies to be set
  parameter: string, a message about the operation
  parameter: object or null, a user object, if the login was successful, or nothing if not

*/
exports.login = async function(callObject, resultCallback) {

  var email = callObject.payload.email;
  var user = new dataInstance.user();
  user = await user.findFirst("email", email);

  //We'll only save this if the login fails.
  var failedLogin = new dataInstance.failedLogin();
  failedLogin.data.ip = callObject.request.connection.remoteAddress;
  failedLogin.data.email = callObject.payload.email;
  failedLogin.data.time = u.makeSQLDatetime();

  if (!user.id) {

    var save = await failedLogin.save();

    if (save !== true) {
      resultCallback(false, 500, [], "A database error occurred.", save);
    } else {
      resultCallback(false, 404, [], "The database looked for a user with the email " + email + ", but found nothing.", null);
    }

  } else {

    var hashedLoginPassword = hashPassword(callObject.payload.password, user.data.salt);

    if (user.data.hashedPassword !== hashedLoginPassword) {

      save = await failedLogin.save();

      if (save !== true) {
        resultCallback(false, 500, [], "A database error occurred.", save);
      } else {
        resultCallback(false, 400, [], "The supplied password doesn't seem to be correct.", null);
      }

    } else {

      //If the password matches, set the access token.
      var laterMultiplier = 1000*60*60*12;

      var createUniqueToken = async function() {
        var random = crypto.randomBytes(128).toString("hex");
        var now = new Date();
        var token = hashPassword(random, now.getTime());

        //This will become the expiration date for the token.
        var later = new Date(now.getTime() + laterMultiplier);
        later = u.makeSQLDatetime(later);

        user.data.accessToken = token;
        user.data.tokenExpires = later;
        user.data.lastLoginIP = callObject.request.connection.remoteAddress;
        user.data.lastLoginTime = u.makeSQLDatetime(now);

        var collision = new dataInstance.user();
        collision = await collision.findFirst("accessToken", token);

        //If we make a new token, and it happens to already exist, try again.
        if (collision.id) {
          createUniqueToken();
        }

      };

      createUniqueToken();

      save = await user.save();

      if (save !== true) {
        resultCallback(false, 500, [], "A database error occurred.", save);
      } else {

        //Cookies measure age in seconds, so laterMultiplier has to be divided by 1000.
        var tokenCookie = "cubicAPIToken=" + user.data.accessToken + "; Secure; " + "Max-Age=" + laterMultiplier / 1000 + ";" + " Path=/";
        var allFailedLogins = new dataInstance.failedLogin();
        allFailedLogins = await allFailedLogins.openAll();

        if (!allFailedLogins.instances) {
          resultCallback(false, 500, [], "A database error occurred.", save);
        } else {

          var deleteErrors = [];

          u.forIn(allFailedLogins.instances, async function(index, instance) {
            if (instance.data.ip === callObject.request.connection.remoteAddress || instance.data.email === callObject.payload.email) {
              var remove = await instance.delete();
              if (remove !== true) {
                deleteErrors.push(remove);
              }
            }
          });

          if (deleteErrors[0]) {
            resultCallback(false, 500, [], "A database error occurred.", deleteErrors);
          } else {

            var allRegistrationKeys = new dataInstance.registrationKey();
            allRegistrationKeys = await allRegistrationKeys.openAll();

            if (!allRegistrationKeys.instances) {
              resultCallback(false, 500, [], "A database error occurred.", allRegistrationKeys);
            } else {
              var found = false;

              u.forIn(allRegistrationKeys.instances, function(index, instance) {
                if (instance.data.userID === user.id) {
                  found = true;
                }
              });

              if (found) {
                resultCallback(false, 400, [], "Users with unused registration keys can't login normally. If you didn't receive a registration confirmation email, check your spam folder or ask an admin to search the server's email logs.", null);
              } else {

                var allResetKeys = new dataInstance.passwordResetKey();
                allResetKeys = await allResetKeys.openAll();

                if (!allResetKeys.instances) {
                  resultCallback(false, 500, [], "A database error occurred.", save);
                } else {

                  deleteErrors = [];

                  u.forIn(allResetKeys.instances, async function(index, instance) {
                    if (instance.data.userID === user.id) {
                      var remove = await instance.delete();
                      if (remove !== true) {
                        deleteErrors.push(remove);
                      }
                    }
                  });

                  if (deleteErrors[0]) {
                    resultCallback(false, 500, [], "A database error occurred.", deleteErrors);
                  } else {

                    callObject.user = user;

                    exports.authenticateDevice(callObject, function(success, code, cookies, message, data) {

                      var infoObject = {developerMessage: "If this frontend can't send cookies to the API, you will need to manually store and send the included cubicAPIToken value in an Authorization header. The Authorization header should be of the form: Bearer cubicAPIToken=[value]; cubicAPIDeviceKey=[value]", cubicAPIToken: tokenCookie.split("; ")[0]};

                      if (!success) {
                        resultCallback(success, code, cookies, message, data);
                      } else {
                        resultCallback(true, 200, [tokenCookie], "User " + user.id + " was logged in.", infoObject);
                      }

                    });

                  }

                }

              }

            }

          }

        }

      }

    }

  }

};

/*
authenticateDevice

callObject {
  parameters: array of strings, the "?" variables in an API route
  payload: object, data submitted to the server
  cookies: array of strings, the cookies found in the request
  requestHeaders: object, the headers from the request
  request: object, the complete request received by the server
}

resultCallback: function, a callback
  parameter: boolean, whether the operation was successful
  parameter: integer, the relevant HTTP status code
  parameter: array of strings, the cookies to be set
  parameter: string, a message about the operation
  parameter: null

*/
exports.authenticateDevice = async function(callObject, resultCallback) {

  if (environmentVariables.bypassDeviceAuthentication) {

    resultCallback(true, 200, [], "Device key authorization was bypassed.", null);

    console.log("**********\nWARNING: Device key authorization is bypassed. \n" +
    "This is not recommended, as anyone able to steal a user's login information \n" +
    "can log in without a second authentication factor.\n" +
    "A user claiming to be " + callObject.user.email + " logged in from " + callObject.request.connection.remoteAddress +
    "\nAt server time " + new Date() + "\n**********");

    return false;

  }

  var keyValue = cubicAPIDeviceKeyExtractor(callObject.cookies);

  if (!keyValue) {
    keyValue = "";
  }

  var user = new dataInstance.user();
  user = await user.findFirst("email", callObject.user.data.email);

  if (!user.id) {
    resultCallback(false, 404, [], "No user found with the supplied email.", null);
  } else {

    var deviceKey = new dataInstance.deviceKey();
    deviceKey = await deviceKey.findFirst("key", keyValue);

    if (!deviceKey.id || !deviceKey.data.authorized) {

      if (!deviceKey.id) {

        //Create a device key for the client/ browser used during registration.
        var key = crypto.randomBytes(128).toString("hex");
        var modifier = crypto.randomBytes(64).toString("hex");
        var authorizationCode = crypto.randomBytes(64).toString("hex");
        var finalDeviceKey = hashPassword(key, modifier);
        var finalAuthorizationCode = hashPassword(authorizationCode, modifier);

        deviceKey.data.key = finalDeviceKey;
        deviceKey.data.authorizationCode = finalAuthorizationCode;
        deviceKey.data.authorized = false;
        deviceKey.data.userID = user.id;

        var success = await deviceKey.save();

        if (!success) {
          resultCallback(false, 500, [], "An error occurred during the storage of a deviceKey:" + success);
          return false;
        }

      }

      sendDeviceKeyEmail(deviceKey.data.authorizationCode, callObject.request, callObject.user, function(success, message) {

        var deviceKeyCookie = "cubicAPIDeviceKey=" + deviceKey.data.key + "; Secure; " + "Max-Age=" + (3.154 * 100000000) + ";" + " Path=/";
        var infoObject = {developerMessage: "If this frontend can't send cookies to the API, you will need to manually store and send the included cubicAPIDeviceKey value in an Authorization header. The Authorization header should be of the form: Bearer cubicAPIToken=[value]; cubicAPIDeviceKey=[value]", cubicAPIDeviceKey: deviceKeyCookie.split("; ")[0]};

        resultCallback(false, 403, [deviceKeyCookie], "Your login has been blocked due to an expired, incorrect, unauthorized, or non-existent cubicAPIDeviceKey. (A previously recognized device or browser can become unrecognized if its cookies are cleared, or if a user other than you logs in using that device.) A key has been generated for this device/ browser, and an attempt was made at sending an authorization email:" + message, infoObject);

      });

    } else {
      resultCallback(true, 200, [], "An authorized device key was matched to a user.", null);
    }

  }

};

/*
authenticateWithToken

callObject {
  parameters: array of strings, the "?" variables in an API route
  payload: object, data submitted to the server
  cookies: array of strings, the cookies found in the request
  requestHeaders: object, the headers from the request
}

resultCallback: function, a callback
  parameter: boolean, whether the operation was successful
  parameter: integer, the relevant HTTP status code
  parameter: array of strings, the cookies to be set
  parameter: string, a message about the operation
  parameter: object or null, a partial user object, if the authentication was successful, or nothing if not

*/
exports.authenticateWithToken = async function(callObject, resultCallback) {

  var tokenValue = cubicAPITokenExtractor(callObject.cookies);

  if (!tokenValue) {
    resultCallback(false, 400, [], "No cubicAPIToken cookie was submitted with this request. The token either expired on the client side or never existed in the first place; Try logging in again.", null);
  } else {

    var user = new dataInstance.user();
    user = await user.findAll("accessToken", tokenValue);

    if (!u.length(user.instances)) {
      resultCallback(false, 404, [], "The supplied cubicAPIToken couldn't be matched with an available user.", null);
    } else {

      var foundUser = null;

      u.forIn(user.instances, function(index, instance) {
        foundUser = instance;
      });

      var now = new Date().getTime();
      var tokenExpires = new Date(foundUser.data.tokenExpires).getTime();

      if (now > tokenExpires) {
        resultCallback(false, 403, [], "The supplied cubicAPIToken appears to have expired.", null);
      } else {

          if (foundUser.data.mustChangePassword == 1) {
            resultCallback(false, 403, [], "User " + foundUser.id + " can't use token authentication until they change their password.", null);
          } else {
            callObject.request.headers["user-id"] = foundUser.id;
            callObject.request.headers["is-superuser"] = foundUser.data.isSuperuser;
            resultCallback(true, 200, [], "User " + foundUser.id + " was authenticated via their cubicAPIToken.", null);
          }

      }

    }

  }

};

/*
update

callObject {
  parameters: array of strings, the "?" variables in an API route
  payload: object, data submitted to the server
  cookies: array of strings, the cookies found in the request
  requestHeaders: object, the headers from the request
}

resultCallback: function, a callback
  parameter: boolean, whether the operation was successful
  parameter: integer, the relevant HTTP status code
  parameter: array of strings, the cookies to be set
  parameter: string, a message about the operation
  parameter: object or null, a user object, if the operation was successful, or nothing if not

*/
exports.update = async function (callObject, resultCallback) {

  var user = new dataInstance.user();
  user = await user.open(callObject.parameters[0]);

  if (!user.id) {
    resultCallback(false, 404, [], "No user was found with ID " + callObject.parameters[0], null);
  } else {

    var input = callObject.payload;
    user.data.email = input.email;

    user.data.isSuperuser = input.isSuperuser;
    user.data.mustChangePassword = input.mustChangePassword;

    var saved = await user.save();

    if (saved !== true) {
      resultCallback(false, 500, [], "An error occurred while storing the updated user.", saved);
    } else {
      user.protectSecretFields();
      resultCallback(true, 200, [], "A user was updated.", user);
    }

  }

};

/*
delete

callObject {
  parameters: array of strings, the "?" variables in an API route
  payload: object, data submitted to the server
  cookies: array of strings, the cookies found in the request
  requestHeaders: object, the headers from the request
}

resultCallback: function, a callback
  parameter: boolean, whether the operation was successful
  parameter: integer, the relevant HTTP status code
  parameter: array of strings, the cookies to be set
  parameter: string, a message about the operation
  parameter: null

*/
exports.delete = async function(callObject, resultCallback) {

  var user = new dataInstance.user();
  var tokenValue = cubicAPITokenExtractor(callObject.cookies);
  var users = await user.findAll("accessToken", tokenValue);

  if (!u.length(users.instances)) {
    resultCallback(false, 404, [], "The supplied cubicAPIToken couldn't be matched with an available user.", null);
  } else {

    var foundUser = null;

    u.forIn(users.instances, function(index, instance) {
      foundUser = instance;
    });

    if (!foundUser) {
      resultCallback(false, 404, [], "The supplied cubicAPIToken couldn't be matched with an available user.", null);
    } else {

      user = await user.open(callObject.parameters[0]);

      if (!user.id) {
        resultCallback(false, 404, [], "The user to be deleted couldn't be found.", null);
      } else {

        if (foundUser.id == callObject.parameters[0] && user.data.isSuperuser == true) {
          resultCallback(false, 400, [], "Superusers can't delete themselves via the API.", null);
        } else {

          var deletion = await user.delete();

          if (deletion !== true) {
            resultCallback(false, 500, [], "There was an error while deleting a user: " + deletion, null);
          } else {
            resultCallback(true, 200, [], "User " + user.id + " was deleted from this system.", null);
          }

        }

      }

    }

  }

};

/*
changePassword

callObject {
  parameters: array of strings, the "?" variables in an API route
  payload: object, data submitted to the server
  cookies: array of strings, the cookies found in the request
  requestHeaders: object, the headers from the request
}

resultCallback: function, a callback
  parameter: boolean, whether the operation was successful
  parameter: integer, the relevant HTTP status code
  parameter: array of strings, the cookies to be set
  parameter: string, a message about the operation
  parameter: null

*/
exports.changePassword = async function(callObject, resultCallback) {

  if (callObject.payload.newPassword === callObject.payload.oldPassword) {
    resultCallback(false, 400, [], "The new and old passwords must not be the same.", null);
  } else {

    if (callObject.payload.newPassword !== callObject.payload.repeatNewPassword) {
      resultCallback(false, 400, [], "Both entries for a new password must match.", null);
    } else {

      var user = new dataInstance.user();
      user = await user.findFirst("email", callObject.payload.email);

      if (!user.id) {
          resultCallback(false, 404, [], "No user found with the provided email.", null);
      } else {
        var hashedInput = hashPassword(callObject.payload.oldPassword, user.data.salt);

        if (hashedInput !== user.data.hashedPassword) {
          resultCallback(false, 404, [], "The input doesn't match the current password.", null);
        } else {

          var newHashedPassword = hashPassword(callObject.payload.newPassword, user.data.salt);
          user.data.mustChangePassword = 0;
          user.data.hashedPassword = newHashedPassword;
          var saved = await user.save();

          if (saved !== true) {
            resultCallback(false, 500, [], "There was an error saving the user's new password: " + saved, null);
          } else {
            resultCallback(true, 200, [], "The user's password was updated.", null);
          }

        }

      }

    }

  }

};

/*
changePasswordWithKey

callObject {
  parameters: array of strings, the "?" variables in an API route
  payload: object, data submitted to the server
  cookies: array of strings, the cookies found in the request
  requestHeaders: object, the headers from the request
}

resultCallback: function, a callback
  parameter: boolean, whether the operation was successful
  parameter: integer, the relevant HTTP status code
  parameter: array of strings, the cookies to be set
  parameter: string, a message about the operation
  parameter: null

*/
exports.changePasswordWithKey = async function(callObject, resultCallback) {

  var key = callObject.payload.key;

  if (callObject.payload.newPassword !== callObject.payload.repeatNewPassword) {
    resultCallback(false, 400, [], "The new password and repeated version must match.", null);
  } else {

    var resetKeys = new dataInstance.passwordResetKey();
    resetKeys = await resetKeys.openAll();
    var foundKey = null;

    u.forIn(resetKeys.instances, function(index, resetKey) {
      if (resetKey.data.key === key) {
        foundKey = resetKey;
      }
    });

    if (!foundKey) {
      resultCallback(false, 400, [], "The provided key doesn't match any stored reset keys.", null);
    } else {

      var user = new dataInstance.user();
      user = await user.open(foundKey.data.userID);

      if (!user.id) {
        resultCallback(false, 404, [], "The user associated with the provided reset key couldn't be retrieved.", null);
      } else {

        var newHashedPassword = hashPassword(callObject.payload.newPassword, user.data.salt);
        user.data.hashedPassword = newHashedPassword;
        var save = await user.save();

        if (save !== true) {
          resultCallback(false, 500, [], "There was an error while updating the user's password: " + save, null);
        } else {
          resultCallback(true, 200, [], "User " + user.id + " had their password changed using a password reset key.", null);
        }

      }

    }

  }

};

/*
requireSuperuser

callObject {
  parameters: array of strings, the "?" variables in an API route
  payload: object, data submitted to the server
  cookies: array of strings, the cookies found in the request
  requestHeaders: object, the headers from the request
}

resultCallback: function, a callback
  parameter: boolean, whether the operation was successful
  parameter: integer, the relevant HTTP status code
  parameter: array of strings, the cookies to be set
  parameter: string, a message about the operation
  parameter: null

*/
exports.requireSuperuser = async function (callObject, resultCallback) {
  var user = new dataInstance.user();
  var foundToken = cubicAPITokenExtractor(callObject.cookies);
  user = await user.findAll("accessToken", foundToken);

  if (u.length(user.instances) === 1) {

    var foundUser = null;

    u.forIn(user.instances, function(index, user) {
      foundUser = user;
    });

    if (foundUser.data.isSuperuser == 0) {
      resultCallback(false, 403, [], "User " + foundUser.id + " does not have superuser privileges, which are required for this operation.", null);
    } else {
      resultCallback(true, 200, [], "User " + foundUser.id + " has superuser privileges.", null);
    }

  } else {
    resultCallback(false, 500, [], "The user's identity couldn't be positively determined based on their login token.", null);
  }

};

/*
confirmRegistration

callObject {
  parameters: array of strings, the "?" variables in an API route
  payload: object, data submitted to the server
  cookies: array of strings, the cookies found in the request
  requestHeaders: object, the headers from the request
}

resultCallback: function, a callback
  parameter: boolean, whether the operation was successful
  parameter: integer, the relevant HTTP status code
  parameter: array of strings, the cookies to be set
  parameter: string, a message about the operation
  parameter: null

*/
exports.confirmRegistration = async function (callObject, resultCallback) {

  var key = callObject.payload.key;

//Prevents empty keys from matching null values in the database.
  if (!key) {
    key = "Not Supplied";
  }

  var registrationKey = new dataInstance.registrationKey();
  registrationKey = await registrationKey.findFirst("key", key);

    if (!registrationKey.id) {
      resultCallback(false, 404, [], "The registration key supplied couldn't be found in the database.", null);
    } else {
      var deletion = await registrationKey.delete();

      if (deletion !== true) {
        resultCallback(false, 500, [], "A database error occurred while removing a registration key.", deletion);
      } else {
        resultCallback(true, 200, [], "A registration key was matched. The affected user can now log in normally.", null);
      }

    }

  };

/*
authorizeDevice

callObject {
  parameters: array of strings, the "?" variables in an API route
  payload: object, data submitted to the server
  cookies: array of strings, the cookies found in the request
  requestHeaders: object, the headers from the request
}

resultCallback: function, a callback
  parameter: boolean, whether the operation was successful
  parameter: integer, the relevant HTTP status code
  parameter: array of strings, the cookies to be set
  parameter: string, a message about the operation
  parameter: null

*/

exports.authorizeDevice = async function(callObject, resultCallback) {

  var code = callObject.payload.code;

//If we neglect to do this, SQLite will match an empty string with a deviceKey that has
//a null value for the authorizationCode.

  if (!code) {
    code = "Not Provided";
  }

  var deviceKey = new dataInstance.deviceKey();
  deviceKey = await deviceKey.findFirst("authorizationCode", code);

  if (!deviceKey.id) {
    resultCallback(false, 404, [], "The authorization code supplied didn't match a device authorization key in the database.", null);
  } else {

    deviceKey.data.authorized = true;
    var save = await deviceKey.save();

    if (save !== true) {
      resultCallback(false, 500, [], "An error occurred while authorizing a device key: " + save, null);
    } else {
      resultCallback(true, 200, [], "A device key was authorized. The user with the assigned key can now login using the device/ browser bearing the key. If the device or browser was previously authorized by another user, it is now NOT authorized for that user any longer.", null);
    }

  }

};

/*
passwordResetSendEmail

callObject {
  parameters: array of strings, the "?" variables in an API route
  payload: object, data submitted to the server
  cookies: array of strings, the cookies found in the request
  requestHeaders: object, the headers from the request
  request: object, the entire request received by the server
}

resultCallback: function, a callback
  parameter: boolean, whether the operation was successful
  parameter: integer, the relevant HTTP status code
  parameter: array of strings, the cookies to be set
  parameter: string, a message about the operation
  parameter: null

*/
exports.passwordResetSendEmail = async function(callObject, resultCallback) {

  var email = callObject.payload.email;

  var user = new dataInstance.user();
  user = await user.findFirst("email", email);

  if (!user.id) {
    resultCallback(false, 404, [], "There is no user " + email + " available on this server.", null);
  } else {

    var key = crypto.randomBytes(20).toString("hex");
    var passwordReset = new dataInstance.passwordResetKey();
    passwordReset.data.userID = user.id;
    passwordReset.data.key = key;
    var save = await passwordReset.save();

    if (save !== true) {
      resultCallback(false, 500, [], "There was a database error while saving a password reset key: " + save, null);
    } else {

      sendResetEmail(key, callObject.request, email, function(success, message) {

        if (!success) {
          resultCallback(false, 500, [], "A password reset key was generated, but the confirmation email doesn't appear to have been sent successfully: " + message, null);
        } else {
          resultCallback(true, 200, [], "A password reset key was generated and sent to: " + email + ". " + message, null);
        }

      });

    }

  }

};

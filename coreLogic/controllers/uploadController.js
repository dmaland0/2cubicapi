/****************************************
*****************************************
Dependencies*/

//Builtin And Third-Party Modules
const fs = require("fs");
//Custom Modules
const environmentVariables = require("../../environmentVariables").environmentVariables;

/****************************************
*****************************************
Module Logic*/

/*
upload

callObject {
  parameters: array of strings, the "?" variables in an API route
  payload: object, data submitted to the server
  cookies: array of strings, the cookies found in the request
  requestHeaders: object, the headers from the request
}

resultCallback: function, a callback
  parameter: boolean, whether the operation was successful
  parameter: integer, the relevant HTTP status code
  parameter: array of strings, the cookies to be set
  parameter: string, a message about the operation
  parameter: null, because no data needs to be returned

*/
exports.upload = function (callObject, resultCallback) {

  var basePath = "../uploads/";

  if (environmentVariables.uploadPath) {
    basePath = environmentVariables.uploadPath;
  }

  var counter = 0;

  var fileName = callObject.payload.fileName
  var extensionPosition = fileName.lastIndexOf(".");
  var fileNameBody = fileName.slice(0,extensionPosition);
  var extension = fileName.slice(extensionPosition);

  var writeFile = () => {

    var filePath = basePath + callObject.payload.fileName;

    fs.writeFile(filePath, callObject.payload.file, "base64", function(error) {

    if (error) {
      resultCallback(false, 500, [], "There was a problem while uploading the file: " + error, null);
    } else {
      resultCallback(true, 200, [], "The file: " + callObject.payload.fileName + " was uploaded. It's name may have been changed if another file would have been overwritten.", {finalPath: "/uploads/" + callObject.payload.fileName});
    }

  });

}

  var checkExistence = () => {

    var filePath = basePath + callObject.payload.fileName;

    fs.access(filePath, fs.constants.F_OK, (error) => {
      counter += 1;

      if (error) {
        writeFile();
      } else {
        callObject.payload.fileName = fileNameBody + "." + counter + extension;
        checkExistence();
      }

    });
  }

  checkExistence();

};

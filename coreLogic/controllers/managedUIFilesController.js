/****************************************
*****************************************
Dependencies*/

//Builtin And Third-Party Modules
const u = require("../utilities/jsUtilities").JSUtilities;
//Custom Modules
const dataInstance = require("../dataInstance/coreInstanceDefinitions");

/****************************************
*****************************************
Module Logic*/

//IMPORTANT: For rewrite, makes sure that there is a content field for the file's body to reside in.

/*
getAll

callObject {
  parameters: array of strings, the "?" variables in an API route
  payload: object, data submitted to the server
  cookies: array of strings, the cookies found in the request
  request: object, the entire, raw request received by the server
  requestHeaders: object, the headers from the request
}

resultCallback: function, a callback
  parameter: boolean, whether the operation was successful
  parameter: integer, the relevant HTTP status code
  parameter: array of strings, the cookies to be set
  parameter: string, a message about the operation
  parameter: array of objects, managed UI file objects

*/
exports.getAll = async function(callObject, resultCallback) {

  var managedFiles = new dataInstance.managedUIFile();
  managedFiles = await managedFiles.openAll();
  var length = u.length(managedFiles.instances);

  if (!length) {
    resultCallback(false, 404, [], "No managed UI files were found.", null);
  } else {
    resultCallback(true, 200, [], "Managed UI files were found.", managedFiles);
  }

};

/*
getOne

callObject {
  parameters: array of strings, the "?" variables in an API route
  payload: object, data submitted to the server
  cookies: array of strings, the cookies found in the request
  request: object, the entire, raw request received by the server
  requestHeaders: object, the headers from the request
}

resultCallback: function, a callback
  parameter: boolean, whether the operation was successful
  parameter: integer, the relevant HTTP status code
  parameter: array of strings, the cookies to be set
  parameter: string, a message about the operation
  parameter: object, a managed UI file

*/
exports.getOne = async function(callObject, resultCallback) {

  var managedFile = new dataInstance.managedUIFile();
  managedFile = await managedFile.findFirst("filename", callObject.parameters[0]);

  if (!managedFile.id) {
    resultCallback(false, 404, [], "No managed UI file was found with the name: " + callObject.parameters[0], null);
  } else {
    resultCallback(true, 200, [], "A managed UI file was found.", managedFile);
  }

};

/*
create

callObject {
  parameters: array of strings, the "?" variables in an API route
  payload: object, data submitted to the server
  cookies: array of strings, the cookies found in the request
  request: object, the entire, raw request received by the server
  requestHeaders: object, the headers from the request
}

resultCallback: function, a callback
  parameter: boolean, whether the operation was successful
  parameter: integer, the relevant HTTP status code
  parameter: array of strings, the cookies to be set
  parameter: string, a message about the operation
  parameter: object, the submitted payload

*/
exports.create = async function(callObject, resultCallback) {

  var file = new dataInstance.managedUIFile();
  file = await file.open(null, callObject.payload.name);

  if (file.id) {
    resultCallback(false, 400, [], "Managed UI files must all have unique names. A record with the specified name already exists.", file);
  } else {

    file.data.filename = callObject.payload.name;
    file.data.lastUpdate = u.makeSQLDatetime();
    file.data.lastUpdatedBy = callObject.request.headers["user-id"];
    file.data.content = callObject.payload.content;
    file.data.published = 0;
    var save = await file.save();

    if (save !== true) {
      resultCallback(false, 500, [], "There was an error while saving the managed UI file: " + save, null);
    } else {
      resultCallback(true, 200, [], "A managed UI file was created.", null);
    }

  }

};

/*
update

callObject {
  parameters: array of strings, the "?" variables in an API route
  payload: object, data submitted to the server
  cookies: array of strings, the cookies found in the request
  request: object, the entire, raw request received by the server
  requestHeaders: object, the headers from the request
}

resultCallback: function, a callback
  parameter: boolean, whether the operation was successful
  parameter: integer, the relevant HTTP status code
  parameter: array of strings, the cookies to be set
  parameter: string, a message about the operation
  parameter: object, the submitted payload

*/
exports.update = async function(callObject, resultCallback) {

  var file = new dataInstance.managedUIFile();
  file = await file.findFirst("fileName", callObject.parameters[0]);

  if (!file.id) {
    resultCallback(false, 404, [], "There is no managed UI file with that name.", null);
  } else {

    file.data.lastUpdate = u.makeSQLDatetime();
    file.data.lastUpdatedBy = callObject.request.headers["user-id"];
    file.data.content = callObject.payload.content;
    file.data.published = callObject.payload.published;
    var save = await file.save();

    if (save !== true) {
      resultCallback(false, 500, [], "There was an error while saving the managed UI file: " + save, null);
    } else {
      resultCallback(true, 200, [], "A managed UI file was modified.", null);
    }

  }

};

/*
delete

callObject {
  parameters: array of strings, the "?" variables in an API route
  payload: object, data submitted to the server
  cookies: array of strings, the cookies found in the request
  request: object, the entire, raw request received by the server
  requestHeaders: object, the headers from the request
}

resultCallback: function, a callback
  parameter: boolean, whether the operation was successful
  parameter: integer, the relevant HTTP status code
  parameter: array of strings, the cookies to be set
  parameter: string, a message about the operation
  parameter: null

*/
exports.delete = async function(callObject, resultCallback) {

  var file = new dataInstance.managedUIFile();
  file = await file.findFirst("fileName", callObject.parameters[0]);

  if (!file.id) {
    resultCallback(false, 404, [], "There is no managed UI file with that name.", null);
  } else {

    var deletion = await file.delete();

    if (deletion !== true) {
      resultCallback(false, 500, [], "There was an error while deleting the managed UI file: " + deletion, null);
    } else {
      resultCallback(true, 200, [], "A managed UI file was deleted.", null);
    }

  }

};

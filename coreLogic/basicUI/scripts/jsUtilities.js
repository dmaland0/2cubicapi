//JSUtilities is a "library" of Javascript functionalities that add useful behaviors and automate repetitive tasks.
//There is a particular emphasis on compatibility, meaning that basic Javascript is used as much as possible.

//Authored by dmaland0@gmail.com

JSUtilities = {
  
  /*
  forIn

  collection: An array, object, or integer to iterate over.

  callback: A function to call at each iteration.
    The first parameter to the callback is always an index or an error message.
    The second parameter is the value corresponding to the index, if applicable.

  range (optional): For arrays, allows for starting the iteration at the index indicated by range.start and ending at range.stop.
  */
  forIn: function(collection, callback, range) {
    var iterator = 0;

    if (!range) {

      range = {
        start: 0,
        stop: null
      };

    }

    if (collection instanceof Array || typeof collection == "string") {

      var maxRange = collection.length - 1;

      if (range.start > maxRange) {
        range.start = maxRange;
      }

      if (range.start < 0) {
        range.start = 0;
      }

      if (range.stop > maxRange || range.stop == null) {
        range.stop = maxRange;
      }

      for (iterator = range.start; iterator <= range.stop; iterator++) {
        callback (iterator, collection[iterator]);
      }

      return true;

    }

    if (collection instanceof Object) {

      for (property in collection) {

        if (collection.hasOwnProperty(property)) {
          callback(property, collection[property]);
        }

      }

      return true;

    }

    if (parseInt(collection)) {

      for (iterator = 0; iterator < collection; iterator++) {
        callback (iterator);
      }

      return true;

    }

    callback("Didn't receive a valid array, object, or range to iterate over.");

  },

  /*
  valueIsIn

  value: What to look for.

  collection: An array or object to search.

  Returns an object indicating whether the value was found or not, and where.
  */
  valueIsIn: function(value, collection) {
    var found = {
      found: false,
      foundAt: null
    };

    if (collection instanceof Array || collection instanceof Object || typeof collection == "string") {

      this.forIn(collection, function(index, valueAtIndex) {

        if (valueAtIndex != null && value === valueAtIndex) {
          found.found = true;
          found.foundAt = index;
        }

      });

    } else {
      found.foundAt = "Error: The collection parameter doesn't appear to be iterable.";
    }

    return found;

  },

  /*
  emailSeemsValid

  email: An email address to test.

  Returns true or false.

  WARNING: EmailSeemsValid doesn't test for every possible violation of the email address spec. It only looks for:
  Anything without spaces, the @ symbol, anything without spaces, a period, and anything without spaces having at least two characters.
  */
  emailSeemsValid: function(email) {

    const emailPattern = /[^ ]+@[^ ]+\.[^ ]{2,}/y;
    return emailPattern.test(email);

  },

  /*
  makeSQLDatetime

  date: Anything that can be used by Javascript to construct a new Date object.

  Returns a SQL datetime string.
  */
  makeSQLDatetime: function(date) {

    if (!date) {
      date = new Date();
    }

    var time = new Date(date);
    var timeComponents = [time.getFullYear(), time.getMonth()+1, time.getDate(), time.getHours(), time.getMinutes(), time.getSeconds()];

    var timeString = "*0-*1-*2 *3:*4:*5";

    this.forIn(timeComponents, function(index, component) {
      //Put in leading zeroes.
      if (component.toString().length < 2) {
        component = "0" + component.toString();
      }

      timeString = timeString.replace("*" + index, component.toString());

    });

    return timeString;

  },

  /*
  iterableParser

  Used to turn an iterable into a human-readable string representation of itself. Does NOT perform nested iterations - this only works one-level deep.

  iterable: Anything that can be iterated over for parsing.

  Returns an array of strings.
  */
  iterableParser: function(iterable) {

    var strings = [];

    if (iterable instanceof Array || iterable instanceof Object || typeof iterable == "string") {

      this.forIn(iterable, function(index, value) {

        var string = "";
        string += index + ": ";
        string += value.toString();
        strings.push(string);

      });

    } else {
      strings.push("Couldn't iterate over the supplied parameter in a meaningful way.");
    }

    return strings;

  },

  /*
  stripAllButLetters

  input: A string, or anything that can be turned into a string.

  Returns a string with only alphabet characters (A-Za-z).
  */
  stripAllButLetters: function(input) {

    input = input.toString();

    var output = input.replace(/[^A-Za-z]/g, "");

    return output;

  },

  /*
  stripAllButNumbers

  input: A string, or anything that can be turned into a string.

  Returns a string with only numerical characters (0-9).
  */
  stripAllButNumbers: function(input) {

    input = input.toString();

    var output = input.replace(/[^0-9]/g, "");

    return output;

  },

  /*
  stripAllButNumericalFormatCharacters

  input: A string, or anything that can be turned into a string.

  Returns a string with only numerical format characters (0-9.,).
  */
  stripAllButNumericalFormatCharacters: function(input) {

    input = input.toString();

    var output = input.replace(/[^0-9.,]/g, "");

    return output;

  },

  /*
  stripSpecifiedFromInput

  specified: a string denoting characters, or a range of characters to be removed from the input.
  input: A string, or anything that can be turned into a string.

  Returns a string with the specified characters removed.
  */
  stripSpecifiedFromInput: function(specified, input) {

    input = input.toString();
    specified = "[" + specified + "]";
    var expression = new RegExp(specified, 'g');

    var output = input.replace(expression, "");

    return output;

  },

  /*
  formatPhoneNumber

  phone: a string that is meant to be a 10-digit USA phone number.
  optionalSeparator: the character to use as a separator, if the traditional (###) ###-#### is not desired.

  Returns a formatted string.
  */
  formatPhoneNumber: function(phone, optionalSeparator) {

    phone = this.stripAllButNumbers(phone);
    var output = "";

    if (!optionalSeparator) {

        this.forIn (phone, function(index, digit) {

          if (index == 0) {
              output += "(";
          }

          if (index == 3) {
            output += ") ";
          }

          if (index == 6) {
            output += "-";
          }

          output += digit;

        });

    } else {

      this.forIn (phone, function(index, digit) {

        if (index == 3 || index == 6) {
          output += optionalSeparator;
        }

        output += digit;

      });

    }

    return output;

  },
  
};

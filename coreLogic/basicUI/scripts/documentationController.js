var documentationController = new zFrame("documentationController");

documentationController.allRoutes = [];

documentationController.buildDocumentationView = function() {

  function createRoute (element, index, route) {

    documentationController.allRoutes.push(element);

    element.route = route;
    element.routePath.innerHTML = route.route;
    element.routeDescription.innerHTML = route.description;

    //Attach data to the button in order to make per-button implementations easier.
    element.tryItButton.route = route;
    element.tryItButton.parameters = [];
    element.tryItButton.fields = [];
    element.tryItButton.verb = element.verb;
    element.tryItButton.responseDisplay = element.responseDisplay;

    var parameterMatches = route.route.match(/\?/g);

    if (parameterMatches) {

      u.forIn(parameterMatches.length, function(index, character) {

        var label = document.createElement("label");
        label.innerHTML = "Parameter " + (element.tryItButton.parameters.length + 1);
        var input = document.createElement("input");
        input.className = "formControl";

        element.parametersForm.appendChild(label);
        element.parametersForm.appendChild(input);
        element.parametersForm.style.display = null;

        //Add the parameter fields to the button, so they're easy to access.
        element.tryItButton.parameters.push(input);
        return character;

      });

    }

    if (route.requiredFields && route.requiredFields.length > 0) {

      u.forIn(route.requiredFields, function(index, field) {

        var label = document.createElement("label");
        label.innerHTML = field;
        var input = document.createElement("input");
        input.className = "formControl";
        input.name = field;

        element.payloadForm.appendChild(label);
        element.payloadForm.appendChild(input);

        //Add the payload fields to the button, so they're easy to access.
        element.tryItButton.fields.push(input);

      });

      element.payloadForm.style.display = null;

    }

    element.viewRoute.targetWrapper = element.interactionWrapper;

    element.viewRoute.onclick = function() {

      var the = element.viewRoute.targetWrapper;

      if (the.style.display === "none") {
        the.style.display = null;
      } else {
        the.style.display = "none";
      }

    };

    element.tryItButton.onclick = function(event) {

      var theButton = event.target;

      event.preventDefault();
      theButton.URL = theButton.route.route;

      u.forIn(theButton.parameters, function(index, parameter) {
        theButton.URL = theButton.URL.replace("?", parameter.value);
      });

      var payload = {};

      if (theButton.fields.length > 0) {

        u.forIn(theButton.fields, function (index, field) {
          payload[field.name] = field.value;
        });

      }

      payload = JSON.stringify(payload);

      //This value is determined on the server side when this script is rendered.
      var cubePath = "<JITRenderCubePath>";

      var URL = null;

      if (cubePath === "undefined") {
        URL = "/api/" + theButton.URL;
      } else {
        URL = cubePath + "api/" + theButton.URL;
      }

      var startTime = new Date().getTime();

      zRequest(theButton.verb, URL, "application/json", [], payload, true, function(xhr) {
        var stopTime = new Date().getTime();
        var totalTime = stopTime - startTime;
        theButton.responseDisplay.value = "Endpoint: " + xhr.responseURL + "\nStatus Code: " + xhr.status +  "\nElapsed Time (ms): " + totalTime + "\n\n" + xhr.response;
      });

    };

  }

  var the = documentationController;

  function makeGroupButtons(buttonTemplate, routeGroups) {

    the.repeatTemplate(buttonTemplate, routeGroups, function(element, index, groupName) {

      element.innerHTML = groupName;
      element.groupName = groupName;
      element.chosen = false;

      element.onclick = function(event) {

        u.forIn (event.target.parentElement.children, function(index, theElement) {

          if (theElement.chosen && theElement !== event.target) {
            theElement.classList.remove("chosen");
            theElement.chosen = false;
          }

        });

        if (!event.target.chosen) {

          event.target.classList.add("chosen");
          event.target.chosen = true;

          u.forIn(event.target.parentElement.parentElement.children, function(index, anElement) {

            if (anElement.route) {

              if (anElement.route.routeRoot === event.target.groupName) {
                anElement.classList.remove("hidden");
              } else {
                anElement.classList.add("hidden");
              }

            }

          });

        } else {

          //In this case, the button is being toggled off without a new group being chosen.
          event.target.classList.remove("chosen");
          event.target.chosen = false;

          u.forIn(event.target.parentElement.parentElement.children, function(index, anElement) {

            if (anElement.route) {

              anElement.classList.remove("hidden");

            }

          });

        }

      };

    });

  }

  the.repeatTemplate(the.GetRoute, the.getRoutes, function(element, index, route) {
    element.verb = "GET";
    createRoute(element, index, route);
  });

  makeGroupButtons(the.GetGroupButton, the.getRouteGroups);

  the.repeatTemplate(the.PostRoute, the.postRoutes, function(element, index, route) {
    element.verb = "POST";
    createRoute(element, index, route);
  });

  makeGroupButtons(the.PostGroupButton, the.postRouteGroups);

  the.repeatTemplate(the.PutRoute, the.putRoutes, function(element, index, route) {
    element.verb = "PUT";
    createRoute(element, index, route);
  });

  makeGroupButtons(the.PutGroupButton, the.putRouteGroups);

  the.repeatTemplate(the.DeleteRoute, the.deleteRoutes, function(element, index, route) {
    element.verb = "DELETE";
    createRoute(element, index, route);
  });

  makeGroupButtons(the.DeleteGroupButton, the.deleteRouteGroups);

};

documentationController.initialize = function() {

  //This value is determined on the server side when this script is rendered.
  var cubePath = "<JITRenderCubePath>";

  var URL = null;

  if (cubePath !== "undefined") {
    URL = cubePath + "api/documentation/";
  } else {
    URL = "/api/documentation/";
  }

  zRequest("GET", URL, "application/json", [], "{}", true, function(xhr) {

    var response = JSON.parse(xhr.response);
    var the = documentationController;
    var those = response.data;

    the.getRouteGroups = [];
    the.postRouteGroups = [];
    the.putRouteGroups = [];
    the.deleteRouteGroups = [];

    if (response.success) {

      var buildRouteGroups = function (routes, routeGroups) {

        u.forIn(routes, function(index, route) {

          route.routeRoot = route.route.split("/")[0];

          if (!u.valueIsIn(route.routeRoot, routeGroups).found) {
            routeGroups.push(route.routeRoot);
          }

        });

      }

      buildRouteGroups(those.getRoutes, the.getRouteGroups);
      buildRouteGroups(those.postRoutes, the.postRouteGroups);
      buildRouteGroups(those.putRoutes, the.putRouteGroups);
      buildRouteGroups(those.deleteRoutes, the.deleteRouteGroups);

      the.getRoutes = those.getRoutes;
      the.postRoutes = those.postRoutes;
      the.putRoutes = those.putRoutes;
      the.deleteRoutes = those.deleteRoutes;

      the.buildDocumentationView();

    }

  });

}

documentationController.routeFilter = function() {

  var filter = documentationController.Filter.value;

  u.forIn(documentationController.allRoutes, function(index, route) {

    var routeMatch = route.route.route.indexOf(filter);
    var descriptionMatch = route.route.description.indexOf(filter);

    if (routeMatch < 0 && descriptionMatch < 0) {
      route.style.display = "none";
    } else {
      route.style.display = null;
    }

  });

};

documentationController.attachUpdateEventsToInputs(documentationController.routeFilter);

documentationController.GetHeader.onclick = function() {
  documentationController.GetRoutesWrapper.showHide();
};

documentationController.PostHeader.onclick = function() {
  documentationController.PostRoutesWrapper.showHide();
};
documentationController.PutHeader.onclick = function() {
  documentationController.PutRoutesWrapper.showHide();
};
documentationController.DeleteHeader.onclick = function() {
  documentationController.DeleteRoutesWrapper.showHide();
};

documentationController.initialize();

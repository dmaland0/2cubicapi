var registerController = new zFrame("registerController");

registerController.validate = function() {

	var the = registerController;
	var allValid = true;

	u.forIn(the, function(index, element) {

		if (element && element.tagName === "INPUT") {

			if (element.value.length < 1) {
				allValid = false;
				element.addClass("invalid");
			} else {
				element.removeClass("invalid");
			}

		}

	});

	if (!u.emailSeemsValid(the.Email.value)) {

		allValid = false;
		the.Email.addClass("invalid");

	} else {
		the.Email.removeClass("invalid");
	}

	if (the.RepeatPassword.value !== the.Password.value) {

		allValid = false;
		the.RepeatPassword.addClass("invalid");

	} else {
		the.RepeatPassword.removeClass("invalid");
	}

	if (allValid) {
		the.Submit.disabled = false;
	} else {
		the.Submit.disabled = true;
	}

};

registerController.Submit.onclick = function(event) {

	event.preventDefault();

	var payload = {};

	payload.email = registerController.Email.value;
	payload.password = registerController.Password.value;
	payload.repeatPassword = registerController.RepeatPassword.value;

	payload = JSON.stringify(payload);

	zRequest("POST", window.location.origin + "/api/users/", "application/json", [], payload, true, function(xhr) {

		var response = JSON.parse(xhr.response);
		alert(response.message);

	});

};

registerController.attachUpdateEventsToInputs(registerController.validate);

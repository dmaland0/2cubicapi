var registrationConfirmationController = new zFrame("registrationConfirmationController");

registrationConfirmationController.confirm = function() {

  var key = window.location.search.substr(2);

  var payload = {};
  payload.key = key;

  payload = JSON.stringify(payload);

  zRequest("PUT", window.location.origin + "/api/users/confirmRegistration/", "application/json", [], payload, true, function(xhr) {
    var response = JSON.parse(xhr.response);
    registrationConfirmationController.Message.innerHTML = response.message;

    if (xhr.status === 200) {

      window.setTimeout(function() {
        window.location.assign(window.location.origin);
      }, 3000);

    }

  });

};

registrationConfirmationController.confirm();

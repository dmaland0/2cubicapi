/****************************************
*****************************************
Dependencies*/

//Builtin And Third-Party Modules
const fs = require("fs");
const sqlite3 = require("../node_modules/sqlite3");
const u = require("../utilities/jsUtilities").JSUtilities;
//Custom Modules
const connections = require("../../dataInstanceDatabaseConnections");

/****************************************
*****************************************
Module Logic*/

exports.databases = {};
exports.ready = false;

function makeConnections(connections) {

  var counter = 0;

  u.forIn(connections, function() {
    counter += 1;
  });

  var pointer = 0;

  u.forIn(connections, function(index, connection) {

    fs.access(connection.path, fs.constants.F_OK, function(accessError) {

      var database = new sqlite3.Database(connection.path, function(error) {
        if (error) {
          console.log("There was an error while opening " + connection + ": " + error);
        } else {

          if (accessError && connection.createStatements) {

            u.forIn(connection.createStatements, function(index, statement) {
              database.run(statement, function(error) {
                if (error) {
                  console.log("There was an error while creating a database structure: " + error);
                }
              });
            });

          }
          exports.databases[index] = database;
          console.log("Connected to " + database.filename);
          pointer += 1;

          if (pointer >= counter) {
            exports.ready = true;
          }

        }

      });

    });

  });

}

makeConnections(connections);

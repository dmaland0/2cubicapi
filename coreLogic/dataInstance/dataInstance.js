/****************************************
*****************************************
Dependencies*/

//Builtin And Third-Party Modules
const u = require("../utilities/jsUtilities").JSUtilities;
//Custom Modules
const databases = require("./sqliteConnectionBuilder").databases;
const connections = require("./sqliteConnectionBuilder");
const environmentVariables = require("../../environmentVariables").environmentVariables;

/****************************************
*****************************************
Module Logic*/

var structureCache = {

};

var building = false;

function buildStructureCache() {

  if (connections.ready) {

    building = true;
    console.log("Building the structure cache...");

    clearInterval(interval);

    u.forIn(databases, function(name, database) {
      structureCache[name] = {};
      database.all("SELECT tbl_name, sql FROM sqlite_master WHERE type = \"table\" AND tbl_name != \"sqlite_sequence\"", function (error, data) {

        if (error) {
          console.log(error);
        } else {

          u.forIn(data, function(index, table) {
            var columnsStatement = table.sql.match(/\(.*\)/gi)[0];
            columnsStatement = columnsStatement.substr(1, columnsStatement.length - 2);
            var columnDefinitions = columnsStatement.split(", ");
            structureCache[name][table.tbl_name] = {};
            u.forIn(columnDefinitions, function(index, definition) {
              var firstSpace = definition.indexOf(" ");
              var fieldName = definition.substr(0, firstSpace);
              var sql = definition.substr(firstSpace + 1);
              structureCache[name][table.tbl_name][fieldName] = sql;
            });

          });

          console.log("Structure Cache built for " + database.filename);

        }

      });

    });

  }

  if (!building) {
    console.log("Waiting to build the structure cache...");
  }

}

if (!environmentVariables.disableStructureCache) {
  var interval = setInterval(buildStructureCache, 100);
}

var fieldWriter = function(instance, dBRow) {

  u.forIn(dBRow, function(index, value) {

    var primaryKey = "id";

    if (instance.primaryKeyColumn) {
        primaryKey = instance.primaryKeyColumn;
    }

    if (index !== primaryKey) {
      instance.data[index] = value;
    }
  });

};

exports.dataInstance = function(databaseName, tableName) {

  var dataInstance = {
    data: {},
    retrievedFromDatabase: false,
    secretFieldsProtected: false,
    primaryKeyColumn: "id"
  };

  dataInstance.database = databases[databaseName];
  dataInstance.table = tableName;

  if (structureCache[databaseName]) {
    if (structureCache[databaseName][tableName]) {
      dataInstance.structure = structureCache[databaseName][tableName];
    }
  }

  dataInstance.open = async function(id) {

    var select = dataInstance.database.prepare("SELECT * FROM " + dataInstance.table + " WHERE " + dataInstance.primaryKeyColumn + " = ?");

    const promise = new Promise((resolve, reject) => {

      select.all([id], function(error, data) {
        if (error) {
          reject(error);
        } else {

          if (data[0]) {
            dataInstance[dataInstance.primaryKeyColumn] = data[0][dataInstance.primaryKeyColumn];
            fieldWriter(dataInstance, data[0]);
            dataInstance.retrievedFromDatabase = true;
            resolve(dataInstance);

          } else {
            resolve(dataInstance);
          }

        }

      });

    });

    const result = await promise;
    return result;

  };

  dataInstance.openLast = async function() {

    var select = dataInstance.database.prepare("SELECT * FROM " + dataInstance.table + " ORDER BY " + dataInstance.primaryKeyColumn + " DESC LIMIT 1");

    const promise = new Promise((resolve, reject) => {

      select.all(function(error, data) {
        if (error) {
          reject(error);
        } else {

          if (data[0]) {

            dataInstance[dataInstance.primaryKeyColumn] = data[0][dataInstance.primaryKeyColumn];
            fieldWriter(dataInstance, data[0]);
            dataInstance.retrievedFromDatabase = true;
            resolve(dataInstance);

          } else {
            resolve(dataInstance);
          }

        }

      });

    });

    const result = await promise;
    return result;

  };

  dataInstance.openAll = async function() {

    var returnObject = {
      totalInDatabase: null,
      instances: {},
      firstInstance: null
    };

    const promise = new Promise((resolve, reject) => {

      var select = dataInstance.database.prepare("SELECT COUNT(*) FROM " + dataInstance.table);

      select.all(function(error, data) {

        if (error) {
          reject(error);
        } else {

          returnObject.totalInDatabase = data[0]["COUNT(*)"];
          var select = dataInstance.database.prepare("SELECT * FROM " + dataInstance.table);

            select.all([], function(error, data) {

              if (error) {
                reject(error);
              } else {

                var first = true;

                u.forIn(data, function(index, row) {

                  var instance = new exports.dataInstance(null);
                  instance.database = dataInstance.database;
                  instance.table = dataInstance.table;
                  instance.primaryKeyColumn = dataInstance.primaryKeyColumn;
                  instance[dataInstance.primaryKeyColumn] = row[dataInstance.primaryKeyColumn];
                  fieldWriter(instance, row);
                  instance.retrievedFromDatabase = true;
                  returnObject.instances[row[dataInstance.primaryKeyColumn]] = instance;

                  if (first) {
                    returnObject.firstInstance = instance;
                    first = false;
                  }

                });

                resolve(returnObject);

              }

            });

        }

      });

    });

    const result = await promise;
    return result;

  };

  dataInstance.openMany = async function(startID, limit) {

    var returnObject = {
      totalInDatabase: null,
      instances: {},
      firstInstance: null
    };

    const promise = new Promise((resolve, reject) => {

      var select = dataInstance.database.prepare("SELECT COUNT(*) FROM " + dataInstance.table);

      select.all(function(error, data) {

        if (error) {
          reject(error);
        } else {

          returnObject.totalInDatabase = data[0]["COUNT(*)"];
          var select = dataInstance.database.prepare("SELECT * FROM " + dataInstance.table + " WHERE " +dataInstance.primaryKeyColumn + " >= ? LIMIT ?;");

          select.all([startID, limit], function(error, data) {

            if (error) {
              reject(error);
            } else {

              var first = true;

              u.forIn(data, function(index, row) {

                var instance = new exports.dataInstance(null);
                instance.database = dataInstance.database;
                instance.table = dataInstance.table;
                instance.secretFields = dataInstance.secretFields;
                instance.primaryKeyColumn = dataInstance.primaryKeyColumn;
                instance[dataInstance.primaryKeyColumn] = row[dataInstance.primaryKeyColumn];
                fieldWriter(instance, row);
                instance.retrievedFromDatabase = true;
                returnObject.instances[row[dataInstance.primaryKeyColumn]] = instance;

                if (first) {
                  returnObject.firstInstance = instance;
                  first = false;
                }

              });

              resolve(returnObject);

            }

          });

        }

      });

    });

    const result = await promise;
    return result;

  };

  dataInstance.findFirst = async function(column, searchValue) {

    var returnObject = {};

    const promise = new Promise((resolve, reject) => {

      var query = "SELECT * FROM " + dataInstance.table + " WHERE " + column + " LIKE '%" + searchValue + "%';";
      var select = dataInstance.database.prepare(query);

      select.all([], function(error, data) {

        if (error) {
          reject(error);
        } else {

          if (data[0]) {
            var theInstance = data[0];
            var instance = new exports.dataInstance(null);
            instance.database = dataInstance.database;
            instance.table = dataInstance.table;
            instance.secretFields = dataInstance.secretFields;
            instance.primaryKeyColumn = dataInstance.primaryKeyColumn;
            instance[dataInstance.primaryKeyColumn] = theInstance[dataInstance.primaryKeyColumn];
            fieldWriter(instance, theInstance);
            instance.retrievedFromDatabase = true;
            returnObject = instance;
          } else {
            var emptyInstance = new exports.dataInstance(null);
            emptyInstance.database = dataInstance.database;
            emptyInstance.table = dataInstance.table;
            emptyInstance.secretFields = dataInstance.secretFields;
            emptyInstance.primaryKeyColumn = dataInstance.primaryKeyColumn;
            emptyInstance[dataInstance.primaryKeyColumn] = null;
            emptyInstance.retrievedFromDatabase = false;
            emptyInstance.data = {};
            returnObject = emptyInstance;
          }

          resolve(returnObject);

        }

      });

    });

    const result = await promise;
    return result;

  };

  dataInstance.findAll = async function(column, searchValue) {

    var returnObject = {
      totalInDatabase: null,
      instances: {},
      firstInstance: null
    };

    const promise = new Promise((resolve, reject) => {

      var select = dataInstance.database.prepare("SELECT COUNT(*) FROM " + dataInstance.table);

      select.all(function(error, data) {

        if (error) {
          reject(error);
        } else {

          returnObject.totalInDatabase = data[0]["COUNT(*)"];
          var query = "SELECT * FROM " + dataInstance.table + " WHERE " + column + " LIKE '%" + searchValue + "%';";
          var select = dataInstance.database.prepare(query);

          select.all([], function(error, data) {

            if (error) {
              reject(error);
            } else {

              var first = true;

              u.forIn(data, function(index, row) {
                var instance = new exports.dataInstance(null);
                instance.database = dataInstance.database;
                instance.table = dataInstance.table;
                instance.secretFields = dataInstance.secretFields;
                instance.primaryKeyColumn = dataInstance.primaryKeyColumn;
                instance[instance.primaryKeyColumn] = row[instance.primaryKeyColumn];
                fieldWriter(instance, row);
                instance.retrievedFromDatabase = true;
                returnObject.instances[row.id] = instance;

                if (first) {
                  returnObject.firstInstance = instance;
                  first = false;
                }

              });

              resolve(returnObject);

            }

          });

        }

      });

    });

    const result = await promise;
    return result;

  };

  dataInstance.save = async function() {

    var command = "";
    var columns = "";
    var parameters = [];

    if (dataInstance.secretFieldsProtected) {
      return "Data instances that have had their secret fields protected can't be saved. You must retrieve an unprotected copy first.";
    }

    if (dataInstance.retrievedFromDatabase) {
      command = "UPDATE " + dataInstance.table + " SET columns WHERE " + dataInstance.primaryKeyColumn + " = ?;"

      u.forIn(dataInstance.data, function(index, value){

        columns += index + "= ?";
        columns += ", ";
        parameters.push(value);

      });

      parameters.push(dataInstance[dataInstance.primaryKeyColumn]);
      //Do a slice to kill the final, extraneous comma.
      command = command.replace("columns", columns.slice(0,-2));
      var sql = dataInstance.database.prepare(command);

    } else {
      command = "INSERT INTO " + dataInstance.table + " (columns) VALUES (valueParameters);"
      var valueParameters = "";

      u.forIn(dataInstance.data, function(index, value){

        columns += index;
        valueParameters += "?";
        columns += ", ";
        valueParameters += ",";
        parameters.push(value);

      });

      //Do slices to kill the final, extraneous comma.
      command = command.replace("columns", columns.slice(0,-2));
      command = command.replace("valueParameters", valueParameters.slice(0,-1));
      var check = dataInstance.database.prepare;
      if (!check) {
        console.log(dataInstance);
      }
      sql = dataInstance.database.prepare(command);

    }

    const promise = new Promise((resolve, reject) => {

      sql.all(parameters, function(error) {
        if (error) {
          reject(error);
        } else {
          resolve(true);
        }

      });

    });

    const result = await promise;
    return result;

  };

  dataInstance.delete = async function() {

    var remove = dataInstance.database.prepare("DELETE FROM " + dataInstance.table  + " WHERE " + dataInstance.primaryKeyColumn + " = ?;");

    const promise = new Promise((resolve, reject) => {

      remove.all([dataInstance[dataInstance.primaryKeyColumn]], function(error) {

        if (error) {
          reject(error);
        } else {
          resolve(true);
        }

      });

    });

    const result = await promise;
    return result;

  };

  dataInstance.customQuery = async function(query, parameterArray) {

    if (!parameterArray) {
      parameterArray = [];
    }

    var command = dataInstance.database.prepare(query);

    const promise = new Promise((resolve, reject) => {

      command.all(parameterArray, function(error, data) {

        if (error) {
          reject(error);
        } else {
          resolve(data);
        }

      });

    });

    const result = await promise;
    return result;

  };

  dataInstance.protectSecretFields = function() {

    u.forIn(dataInstance.data, function(key) {
      if (u.valueIsIn(key, dataInstance.secretFields).found) {
        dataInstance.data[key] = "PROTECTED";
      }
    });

    dataInstance.secretFieldsProtected = true;

  };

  return dataInstance;

};

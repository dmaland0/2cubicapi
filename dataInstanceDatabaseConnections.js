/****************************************
*****************************************
Dependencies*/

//Builtin And Third-Party Modules

//Custom Modules

/****************************************
*****************************************
Module Logic*/

/****************************************
*****************************************
Custom Database Connections*/

/*Example
{
  databaseName: "./path/relative/to/server.js/name.sqlite"
}
The cubicAPI root directory is ../
The coreLogic directory is ./
*/

exports.examples = {
  path: "../databases/examples.sqlite",
  createStatements: ["CREATE TABLE IF NOT EXISTS exampleTable (id INTEGER PRIMARY KEY AUTOINCREMENT, message STRING);"]
};

/****************************************
*****************************************
Core Database Connections*/

exports.core = {
  path: "./databases/core.sqlite",
  createStatements: [
    "CREATE TABLE IF NOT EXISTS users (id INTEGER  PRIMARY KEY AUTOINCREMENT,  email  STRING UNIQUE, isSuperuser  INTEGER, salt STRING, accessToken STRING, tokenExpires DATETIME, lastLoginIP STRING, lastLoginTime DATETIME, hashedPassword STRING, mustChangePassword INTEGER, createdAt DATETIME);",
    "CREATE TABLE If NOT EXISTS deviceKeys (id INTEGER PRIMARY KEY AUTOINCREMENT, userID INTEGER REFERENCES users (id) ON DELETE CASCADE ON UPDATE CASCADE, [key] STRING, authorized INTEGER, authorizationCode STRING);",
    "CREATE TABLE IF NOT EXISTS failedLogins (id INTEGER  PRIMARY KEY AUTOINCREMENT, ip STRING, email STRING, time DATETIME);",
    "CREATE TABLE IF NOT EXISTS managedUIFiles (id INTEGER  PRIMARY KEY AUTOINCREMENT, fileName STRING UNIQUE, content STRING, published INTEGER, lastUpdate DATETIME, lastUpdatedBy INTEGER  REFERENCES users (id) ON DELETE SET NULL ON UPDATE CASCADE);",
    "CREATE TABLE IF NOT EXISTS passwordResetKeys (id INTEGER PRIMARY KEY AUTOINCREMENT, userID INTEGER REFERENCES users (id) ON DELETE CASCADE ON UPDATE CASCADE, [key]  STRING);",
    "CREATE TABLE registrationKeys (id INTEGER PRIMARY KEY AUTOINCREMENT, userID INTEGER REFERENCES users (id) ON DELETE CASCADE ON UPDATE CASCADE, [key]  STRING);",
  ]
};

exports.log = {
    path: "./databases/log.sqlite",
    createStatements: [
      "CREATE TABLE entries (id INTEGER  PRIMARY KEY AUTOINCREMENT, remoteHost  STRING, remotePort  STRING, headersJSON STRING, verb STRING, url STRING, statusCode  INTEGER, createdAt DATETIME);"
    ]
};

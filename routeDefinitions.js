/****************************************
*****************************************
Dependencies*/

//Builtin And Third-Party Modules
//Custom Modules - Load any controllers required by your routes in this section.
const usersController = require("./coreLogic/controllers/usersController");
const exampleController = require("./controllers/exampleController");

/****************************************
*****************************************
Module Logic*/

//This is where you store all your API routing.
//API routes are reached by URLs like: https://opencubes.test/api/route.
//The "route" field is the URL used without any leading "/" characters.
//A "?" in a route is a variable parameter passed within the URL.

/*
Sample get/deleteRoute

{
  route: "hello/?",
  beforeAction: [],
  action: defaultController.sayHello,
  description: "A friendly salutation."
},

*/

/*
Sample post/putRoute

{
  route: "postSomething/",
  beforeAction: [],
  action: defaultController.receivePost,
  requiredFields: ["message"],
  description: "Send a message back to yourself."
},

*/

//VERY IMPORTANT: These route definitions are available to anyone that hits "/api/routeDump/". Don't put anything in them that ought to be a secret!

exports.getRoutes = [

  {
    route: "helloWorld/?",
    beforeAction: [],
    action: exampleController.sayHello,
    description: "Send a URL parameter back to yourself."
  }

];

exports.postRoutes = [

  {
    route: "postTest/",
    beforeAction: [],
    action: exampleController.receivePost,
    requiredFields: ["message"],
    description: "Send a payload back to yourself."
  },

  {
    route: "savePost/",
    beforeAction: [usersController.authenticateWithToken],
    action: exampleController.savePost,
    requiredFields: ["message"],
    description: "Store a payload in the examples database."
  }

];

exports.putRoutes = [

];

exports.deleteRoutes = [

];

module.exports = {
    'env': {
        'browser': true,
        'node': true,
        'es6': true
    },
    'extends': 'eslint:recommended',
    'parserOptions': {
        'ecmaFeatures': {
            'jsx': false
        },
        "ecmaVersion": 2017,
        'sourceType': 'module'
    },
    'rules': {
        'quotes': [2, 'double', 'avoid-escape'],
        'no-console': [0],
        'eqeqeq': [1, 'smart'],
        'brace-style': [2, '1tbs'],
        'array-bracket-spacing': [2, 'never'],
        'camelcase': [2, {'properties': 'never'}],
        'keyword-spacing': [2],
        'eol-last': [2],
        'no-trailing-spaces': [2],
        'no-useless-escape': [1]
    },
    'globals': {
        'zFrame': false,
        'zRequest': false,
        'u': false
    }
}

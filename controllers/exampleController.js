/****************************************
*****************************************
Dependencies*/

//Builtin And Third-Party Modules

//Custom Modules
const dataInstance = require("../instanceDefinitions");

/****************************************
*****************************************
Module Logic*/


/*
sayHello

callObject {
  parameters: array of strings, the "?" variables in an API route
  payload: object, data submitted to the server
  cookies: array of strings, the cookies found in the request
  requestHeaders: object, the headers from the request
  request: object, the complete request received by the server
}

resultCallback: function, a callback
  parameter: boolean, whether the operation was successful
  parameter: integer, the relevant HTTP status code
  parameter: array of strings, the cookies to be set
  parameter: string, a message about the operation
  parameter: object, the request headers received by the server

*/
exports.sayHello = function(callObject, resultCallback) {

  resultCallback(true, 200, ["aCookieForYou=chocolateChip; Path=/"], "Hello there! The URL parameter you passed, if any was: " + callObject.parameters[0], null);

};

/*
receivePost

callObject {
  parameters: array of strings, the "?" variables in an API route
  payload: object, data submitted to the server
  cookies: array of strings, the cookies found in the request
  requestHeaders: object, the headers from the request
  request: object, the complete request received by the server
}

resultCallback: function, a callback
  parameter: boolean, whether the operation was successful
  parameter: integer, the relevant HTTP status code
  parameter: array of strings, the cookies to be set
  parameter: string, a message about the operation
  parameter: object, the payload received by the server

*/
exports.receivePost = function(callObject, resultCallback) {

  resultCallback(true, 200, ["aCookieForYou=chocolateChip; Path=/"], "Your POST request was received by the server.", callObject.payload);

};

/*
savePost

callObject {
  parameters: array of strings, the "?" variables in an API route
  payload: object, data submitted to the server
  cookies: array of strings, the cookies found in the request
  requestHeaders: object, the headers from the request
  request: object, the complete request received by the server
}

resultCallback: function, a callback
  parameter: boolean, whether the operation was successful
  parameter: integer, the relevant HTTP status code
  parameter: array of strings, the cookies to be set
  parameter: string, a message about the operation
  parameter: object, the payload received by the server

*/
exports.savePost = async function(callObject, resultCallback) {

  var example = new dataInstance.example();
  example.data.message = callObject.payload.message;
  var saved = await example.save();

  if (saved !== true) {
    resultCallback(false, 500, [], "There was an error while saving your message: " + saved, null);
  } else {
    resultCallback(true, 200, [], "Your POST request was saved on the server.", example);
  }

};

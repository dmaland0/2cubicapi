/****************************************
*****************************************
Dependencies*/

//Builtin And Third-Party Modules

//Custom Modules
const dataInstance = require("./coreLogic/dataInstance/dataInstance").dataInstance;

/****************************************
*****************************************
Module Logic*/

exports.example = function() {
  //var instance = new dataInstance("database", "table");
  /*
  The "database" parameter is the name of a database from dataInstanceDatabaseConnections.
  Those databases are connected to using exports from the module, so "examples" is a
  reference to exports.examples from dataInstanceDatabaseConnections.
  */
  var instance = new dataInstance("examples","exampleTable");
  instance.secretFields = [];
  return instance;
};
